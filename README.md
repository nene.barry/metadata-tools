# Elixir LU metadata utilities

## Clone the repository

Do not forget to also **clone the submodules!**

```bash
git clone <repository-url>
git submodule update --init
```

## Versions of DISH
Tags are used for keeping track of which code version supported which DISH version.

 Old DISH exporter can produce JSON which is not compatible with newest JSON schemas or Daisy importer. In this case, migration of the DISH is the safest procedure.

 Versions:
 - v.8 -> v.9 - no schema breaking change, update of one text field
 - v.9 -> v.10 - no schema breaking change, just more data use restrictions are collected

## Development environment setup

Create virtual environment:

```bash
mkdir project_venv
python3 -m venv project_venv
source ./project_venv/bin/activate
```

Install dependencies (single brackets are required if you are using `zsh`):

```bash
pip install -e '.[dev]'
```

## Testing

Run tests with:

```bash
python setup.py test
```

## Current Version

**v0.2.0-dev**
