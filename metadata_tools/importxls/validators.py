
import logging
from typing import Text


class DISHVersionValidator():
    _supported_version = 'v.11'
    
    def get_supported_version(self):
        return self._supported_version
    
    def validate_book(self, book):
        dictionary_sheet = book["_Dictionary"]
        dish_version = dictionary_sheet[0,0]
        if not dish_version:
            logging.warning('No version of DISH provided - validation will be skipped.')
            return False
        if self._supported_version != dish_version:
            raise  NotImplementedError(f'Version validation error: DISH has unsupported version \'{dish_version}\'. Supported version is \'{self._supported_version}\'.')
        return True

